package com.example.rogerio.mytodo.frontEndApp.persistence.dao.impl;

import com.example.rogerio.mytodo.frontEndApp.firebase.FirebaseService;
import com.example.rogerio.mytodo.frontEndApp.ofy.OfyService;
import com.example.rogerio.mytodo.frontEndApp.persistence.dao.GenericFirebaseDAO;
import com.example.rogerio.mytodo.frontEndApp.persistence.model.BaseEntity;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.googlecode.objectify.Objectify;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by rogerio on 16/05/16.
 */
public class GenericFirebaseDAOImpl<T extends BaseEntity<ID>, ID extends Serializable>
        implements GenericFirebaseDAO<T, ID> {


    public static class User {

        public String date_of_birth;
        public String full_name;
        public String nickname;

        public User(String date_of_birth, String full_name) {
            this.date_of_birth = date_of_birth;
            this.full_name = full_name;
        }

        public User(String date_of_birth, String full_name, String nickname) {
            this.date_of_birth = date_of_birth;
            this.full_name = full_name;
            this.nickname = nickname;
        }

    }

    private Class<T> clazz;

    private static final Logger log = Logger.getLogger(GenericFirebaseDAOImpl.class.getName());

    private DatabaseReference.CompletionListener complete = new DatabaseReference.CompletionListener() {
        @Override
        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
            if (databaseError != null) {
                System.out.println("Data could not be saved " + databaseError.getMessage());
            } else {
                System.out.println("Data saved successfully.");
            }
        }
    };

    public GenericFirebaseDAOImpl() {
        clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];
        FirebaseService firebaseService = new FirebaseService();
    }

    @Override
    public List<T> findAll() {
        Objectify objectify = OfyService.ofy();
        List<T> entities = objectify.load().type(clazz).list();
        if (entities == null || entities.size() <= 0) {
            return null;
        }
        return entities;
    }

    @Override
    public List<T> findAllActives() {
        return null;
    }

    @Override
    public T findById(ID id) {
        Objectify objectify = OfyService.ofy();
        T entity = null;

        if (id instanceof Long) {
            entity = objectify.load().type(clazz).id((Long) id).now();
        } else if (id instanceof String) {
            entity = objectify.load().type(clazz).id((String) id).now();
        }
        return entity;
    }

    @Override
    public T findByProperty(String property, Object value) {
        Objectify objectify = OfyService.ofy();
        T entity = objectify.load().type(clazz).filter(property, value).first().now();
        return entity;
    }

    @Override
    public String add(T entity, String reference) {

        final FirebaseDatabase database = FirebaseService.getInstance();
        DatabaseReference ref = database.getReference().child(reference);

        ref.setValue("todo1", complete);

        return "sa";
        //DatabaseReference newRef = ref.push();
        //DatabaseReference newRef = FirebaseService.getInstance().getReference().child(reference).push();
        //newRef.setValue(entity);
        //return newRef.getKey();
    }

    @Override
    public boolean update(T entity) {
        Objectify objectify = OfyService.ofy();
        objectify.save().entity(entity).now();
        return true;
    }

    @Override
    public boolean delete(T entity) {
        Objectify objectify = OfyService.ofy();
        objectify.delete().entity(entity).now();

        // if group ID = null, it was deleted
        if (entity.getId() == null) {
            return true;
        } else {
            return false;
        }
    }
}
