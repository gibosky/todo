package com.example.rogerio.mytodo.frontEndApp.persistence.dao;

import com.example.rogerio.mytodo.frontEndApp.persistence.model.MyBean;
import com.googlecode.objectify.Key;

import java.util.List;

/**
 * Created by rogerio on 15/05/16.
 */
public interface MyEndpointDAO extends GenericDAO<MyBean, Long>  {

}
