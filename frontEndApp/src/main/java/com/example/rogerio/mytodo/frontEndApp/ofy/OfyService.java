package com.example.rogerio.mytodo.frontEndApp.ofy;

/**
 * Created by rogerio on 17/05/16.
 */
import com.example.rogerio.mytodo.frontEndApp.persistence.model.MyBean;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by rogerio on 13/05/16.
 */
public class OfyService implements ServletContextListener {


    static {
        ObjectifyService.register(MyBean.class);
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }

    public static Objectify ofy() {

        return ObjectifyService.ofy();
    }


    public static ObjectifyFactory factory() {

        return ObjectifyService.factory();
    }
}
