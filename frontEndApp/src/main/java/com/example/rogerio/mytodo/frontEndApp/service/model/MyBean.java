package com.example.rogerio.mytodo.frontEndApp.service.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

/** The object model for the data we are sending through endpoints */
@Entity
public class MyBean {

    @Id
    public Long id;

    public String myData;

    public String getData() {
        return myData;
    }

    public void setData(String data) {
        myData = data;
    }
}