package com.example.rogerio.mytodo.frontEndApp.firebase;

import com.firebase.client.Firebase;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.FirebaseDatabase;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by rogerio on 17/05/16.
 *
 * public static MyEndpointDAOImpl getInstance() {
 */
public class FirebaseService {

    private static FirebaseDatabase instance = null;

    /*public FirebaseService(){
        FirebaseOptions options = null;
        try {
            options = new FirebaseOptions.Builder()
                    .setServiceAccount(new FileInputStream("WEB-INF/todo-c34a73ed9319.json"))
                    .setDatabaseUrl("https://todo-5846f.firebaseio.com/")
                    .build();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        FirebaseApp firebaseApp = FirebaseApp.initializeApp(options);
    }*/

    public static FirebaseDatabase getInstance() {
        if (instance == null) {

            FirebaseOptions options = null;
            try {
                options = new FirebaseOptions.Builder()
                        .setServiceAccount(new FileInputStream("WEB-INF/todo-c34a73ed9319.json"))
                        .setDatabaseUrl("https://todo-5846f.firebaseio.com/")
                        .build();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            FirebaseApp firebaseApp = FirebaseApp.initializeApp(options);


            instance = FirebaseDatabase.getInstance();
        }
        return  instance;
    }
}
