/*
   For step-by-step instructions on connecting your Android application to this backend module,
   see "App Engine Java Endpoints Module" template documentation at
   https://github.com/GoogleCloudPlatform/gradle-appengine-templates/tree/master/HelloEndpoints
*/

package com.example.rogerio.mytodo.frontEndApp.service.endpoint;

import com.example.rogerio.mytodo.frontEndApp.firebase.FirebaseService;
import com.example.rogerio.mytodo.frontEndApp.persistence.model.MyBean;
import com.example.rogerio.mytodo.frontEndApp.service.MyEndpointService;
import com.example.rogerio.mytodo.frontEndApp.service.impl.MyEndpointServiceImpl;

import com.firebase.client.Firebase;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.NotFoundException;

import javax.inject.Named;

/** An endpoint class we are exposing */
@Api(
  name = "myApi",
  version = "v1",
  namespace = @ApiNamespace(
    ownerDomain = "frontEndApp.mytodo.rogerio.example.com",
    ownerName = "frontEndApp.mytodo.rogerio.example.com",
    packagePath=""
  )
)
public class MyEndpoint {


    /** A simple endpoint method that takes a name and says Hi back */
    @ApiMethod(name = "sayHi")
    public MyBean sayHi(@Named("name") String name) {

        MyEndpointService service = new MyEndpointServiceImpl();
        MyBean myBean = service.sayHi(name);

        return myBean;

    }

    @ApiMethod(name = "getHi")
    public MyBean getHi(@Named("id") Long id) throws NotFoundException {

        MyEndpointService service = new MyEndpointServiceImpl();
        MyBean myBean = service.getHi(id);

        return myBean;

    }

}
