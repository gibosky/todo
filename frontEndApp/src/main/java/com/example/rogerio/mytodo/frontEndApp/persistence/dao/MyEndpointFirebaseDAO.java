package com.example.rogerio.mytodo.frontEndApp.persistence.dao;

import com.example.rogerio.mytodo.frontEndApp.persistence.model.MyBean;

/**
 * Created by rogerio on 15/05/16.
 */
public interface MyEndpointFirebaseDAO extends GenericFirebaseDAO<MyBean, Long>  {

}
