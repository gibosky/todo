package com.example.rogerio.mytodo.frontEndApp.persistence.dao.impl;

import com.example.rogerio.mytodo.frontEndApp.persistence.dao.MyEndpointDAO;
import com.example.rogerio.mytodo.frontEndApp.persistence.model.MyBean;
import com.googlecode.objectify.Key;

import java.util.List;

/**
 * Created by rogerio on 16/05/16.
 */
public class MyEndpointDAOImpl extends GenericDAOImpl<MyBean, Long> implements MyEndpointDAO {

    private static MyEndpointDAOImpl instance;

    private MyEndpointDAOImpl() {}

    public static MyEndpointDAOImpl getInstance() {
        if (instance == null) {
            instance = new MyEndpointDAOImpl();
        }
        return instance;
    }
}
