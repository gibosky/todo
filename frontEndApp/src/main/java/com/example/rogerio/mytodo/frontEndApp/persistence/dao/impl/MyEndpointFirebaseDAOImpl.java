package com.example.rogerio.mytodo.frontEndApp.persistence.dao.impl;

import com.example.rogerio.mytodo.frontEndApp.persistence.dao.MyEndpointDAO;
import com.example.rogerio.mytodo.frontEndApp.persistence.dao.MyEndpointFirebaseDAO;
import com.example.rogerio.mytodo.frontEndApp.persistence.model.MyBean;

/**
 * Created by rogerio on 16/05/16.
 */
public class MyEndpointFirebaseDAOImpl extends GenericFirebaseDAOImpl<MyBean, Long> implements MyEndpointFirebaseDAO {

    private static MyEndpointFirebaseDAOImpl instance;

    private MyEndpointFirebaseDAOImpl() {}

    public static MyEndpointFirebaseDAOImpl getInstance() {
        if (instance == null) {
            instance = new MyEndpointFirebaseDAOImpl();
        }
        return instance;
    }
}
