package com.example.rogerio.mytodo.frontEndApp.service;

import com.example.rogerio.mytodo.frontEndApp.persistence.model.MyBean;
import com.google.api.server.spi.response.NotFoundException;

import javax.inject.Singleton;

/**
 * Created by rogerio on 13/05/16.
 */
@Singleton
public interface MyEndpointService {

    MyBean sayHi (String name);

    MyBean getHi (Long id) throws NotFoundException;
}
