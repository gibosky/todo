package com.example.rogerio.mytodo.frontEndApp.persistence.model;

import com.googlecode.objectify.annotation.Unindex;

import java.util.Date;

/**
 * Created by rogerio on 16/05/16.
 */
public abstract class BaseEntity<ID> {

    @Unindex
    Date inactivatedDate;

    public abstract ID getId();

    public abstract void setId(ID id);

    public Date getInactivatedDate() {
        return inactivatedDate;
    }

    public void setInactivatedDate(Date inactivatedDate) {
        this.inactivatedDate = inactivatedDate;
    }

}
