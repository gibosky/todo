package com.example.rogerio.mytodo.frontEndApp.service.impl;

import com.example.rogerio.mytodo.frontEndApp.firebase.FirebaseService;
import com.example.rogerio.mytodo.frontEndApp.persistence.dao.MyEndpointDAO;
import com.example.rogerio.mytodo.frontEndApp.persistence.dao.MyEndpointFirebaseDAO;
import com.example.rogerio.mytodo.frontEndApp.persistence.dao.impl.MyEndpointDAOImpl;
import com.example.rogerio.mytodo.frontEndApp.persistence.dao.impl.MyEndpointFirebaseDAOImpl;
import com.example.rogerio.mytodo.frontEndApp.persistence.model.MyBean;
import com.example.rogerio.mytodo.frontEndApp.service.MyEndpointService;

import com.firebase.client.Firebase;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.datastore.Entity;

import javax.inject.Singleton;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * Created by rogerio on 13/05/16.
 */
public class MyEndpointServiceImpl implements MyEndpointService {

    MyEndpointDAO myEndpointDAO = MyEndpointDAOImpl.getInstance();
    MyEndpointFirebaseDAO myEndpointFirebaseDAO = MyEndpointFirebaseDAOImpl.getInstance();

    @Override
    public MyBean sayHi(String name) {

        MyBean response = new MyBean();
        response.setData("Hi, " + name);

        Entity data = new Entity("SayHi");
        data.setProperty("name", name);

        myEndpointDAO.add(response);
        String key = myEndpointFirebaseDAO.add(response, "todoItems");

        return response;
    }

    @Override
    public MyBean getHi(Long id) throws NotFoundException {
        final MyBean bean = myEndpointDAO.findById(id);

        if (bean == null) {
            throw new NotFoundException("Bean não encontrado");
        }
        return  bean;
    }
}
