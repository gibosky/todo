package com.example.rogerio.mytodo.frontEndApp.persistence.dao.impl;

import com.example.rogerio.mytodo.frontEndApp.ofy.OfyService;
import com.example.rogerio.mytodo.frontEndApp.persistence.dao.GenericDAO;
import com.example.rogerio.mytodo.frontEndApp.persistence.model.BaseEntity;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by rogerio on 16/05/16.
 */
public class GenericDAOImpl<T extends BaseEntity<ID>, ID extends Serializable>
        implements GenericDAO<T, ID> {

    private Class<T> clazz;

    public GenericDAOImpl() {
        clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];
    }

    @Override
    public List<T> findAll() {
        Objectify objectify = OfyService.ofy();
        List<T> entities = objectify.load().type(clazz).list();
        if (entities == null || entities.size() <= 0) {
            return null;
        }
        return entities;
    }

    @Override
    public List<T> findAllActives() {
        return null;
    }

    @Override
    public T findById(ID id) {
        Objectify objectify = OfyService.ofy();
        T entity = null;

        if (id instanceof Long) {
            entity = objectify.load().type(clazz).id((Long) id).now();
        } else if (id instanceof String) {
            entity = objectify.load().type(clazz).id((String) id).now();
        }
        return entity;
    }

    @Override
    public T findByProperty(String property, Object value) {
        Objectify objectify = OfyService.ofy();
        T entity = objectify.load().type(clazz).filter(property, value).first().now();
        return entity;
    }

    @Override
    public Key<T> add(T entity) {
        Objectify objectify = OfyService.ofy();
        Key<T> key = objectify.save().entity(entity).now();

        return key;
    }

    @Override
    public boolean update(T entity) {
        Objectify objectify = OfyService.ofy();
        objectify.save().entity(entity).now();
        return true;
    }

    @Override
    public boolean delete(T entity) {
        Objectify objectify = OfyService.ofy();
        objectify.delete().entity(entity).now();

        // if group ID = null, it was deleted
        if (entity.getId() == null) {
            return true;
        } else {
            return false;
        }
    }
}
