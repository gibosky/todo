package com.example.rogerio.mytodo.frontEndApp.persistence.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * Created by rogerio on 16/05/16.
 */
@Entity
public class MyBean extends BaseEntity<Long> {

    @Id
    private Long id;

    @Index
    private String data;

    @Index
    private Long value;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }


    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }
}
