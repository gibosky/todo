package com.example.rogerio.mytodo.frontEndApp.service;

import com.example.rogerio.mytodo.frontEndApp.persistence.model.MyBean;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.util.Closeable;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by rogerio on 01/06/16.
 */
public class MyEndpointServiceTest {

    private final LocalServiceTestHelper helper =
            new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());

    private Closeable closeable;
    private DatastoreService ds;

    @Before
    public void setUp() throws Exception {

        helper.setUp();
        ds = DatastoreServiceFactory.getDatastoreService();

        ObjectifyService.register(MyBean.class);

        closeable = ObjectifyService.begin();

        cleanDatastore(ds, "default");
    }

    @After
    public void tearDown() {
        cleanDatastore(ds, "default");
        helper.tearDown();
        closeable.close();
    }

    @Test
    public void createSaveObject() throws Exception {

        MyBean bean = new MyBean();
        bean.setData("rogerio");
        bean.setValue(1l);
        ObjectifyService.ofy().save().entity(bean).now();

        Query.Filter propertyFilter =
                new Query.FilterPredicate("value", Query.FilterOperator.EQUAL, 1l);

        Query query = new Query("MyBean").setFilter(propertyFilter);
        PreparedQuery pq = ds.prepare(query);
        Entity admissaoResult = pq.asSingleEntity();    // Should only be one at this point.
        assertEquals(admissaoResult.getProperty("data"), "rogerio");
    }

    public static void cleanDatastore(DatastoreService ds, String book) {
        Query query = new Query("MyBean").setKeysOnly();
        PreparedQuery pq = ds.prepare(query);
        List<Entity> entities = pq.asList(FetchOptions.Builder.withDefaults());
        ArrayList<Key> keys = new ArrayList<>(entities.size());

        for (Entity e : entities) {
            keys.add(e.getKey());
        }
        ds.delete(keys);
    }

}
